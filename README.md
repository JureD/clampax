# Collection of cLAMPAx container images

## Disclamer
**This project is a work in progress:** Sudden feature-breaking changes may occur without prior notice.

## Image genealogy

```mermaid
graph TD;
    cLAMPAx-->cLAMPAx-gui;
    cLAMPAx-->cLAMPAx-ros2;
    cLAMPAx-ros2-->cLAMPAx-ros2-gui;
```

| cLAMPAx version | Base image | Additional packages | Features | Recommended usage |
| :-- | :-- | :-- | :-- | :-- |
| cLAMPAx  | ubuntu:22.04 | sudo<br>tmux<br>neovim<br>nano<br>zsh<br>git<br>openssh-server | &bull; SSH access<br>&bull; non root user: lampa | [See: Usage - cLAMPAx](#clampax) |
| cLAMPAx-gui  | cLAMPAx | tigervnc-standalone-server<br>openbox<br>xterm<br>novnc | &bull; SSH access<br>&bull; non root user: lampa<br>&bull; VNC + noVNC + minimal desktop environment: Openbox | [See: Usage - cLAMPAx:gui](#clampaxgui) |
| cLAMPAx-ros2  | cLAMPAx | ros-humble-ros-base<br>ros-dev-tools | &bull; SSH access<br>&bull; non root user: lampa<br>&bull; minimal-ish ROS2 environment | [See: Usage - cLAMPAx:ros2](#clampaxros2) |
| cLAMPAx-ros2-gui  | cLAMPAx-ros2 | tigervnc-standalone-server<br>openbox<br>xterm<br>novnc<br>ros-humble-desktop | &bull; SSH access<br>&bull; non root user: lampa<br>&bull; VNC + noVNC + minimal desktop environment: Openbox<br>&bull; full ROS2 environment | [See: Usage - cLAMPAx:ros2-gui](#clampaxros2-gui) |

## Usage

### cLAMPAx

```bash
docker run -p 6022:6022 registry.gitlab.com/jured/clampax zsh
```

### cLAMPAx:gui

for use with noVNC:
```shell
docker run -p 6022:6022 -p 6080:6080 registry.gitlab.com/jured/clampax:gui
```

or for use with other VNC cliets:

```shell
docker run -p 6022:6022 -p 5901:5901 registry.gitlab.com/jured/clampax:gui
```

### cLAMPAx:ros2

```shell
docker run -p 6022:6022 registry.gitlab.com/jured/clampax:ros2 zsh
```

### cLAMPAx:ros2-gui

for use with noVNC:
```shell
docker run -p 6022:6022 -p 6080:6080 registry.gitlab.com/jured/clampax:ros2-gui
```

or for use with other VNC cliets:

```shell
docker run -p 6022:6022 -p 5901:5901 registry.gitlab.com/jured/clampax:ros2-gui
```