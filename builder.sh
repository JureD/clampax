#!/usr/bin/env bash

REGISTRY_URL=registry.gitlab.com/jured/clampax

for TAG in "" ":gui" ":ros2" ":ros2-gui";
do
    echo "--> Building $REGISTRY_URL$TAG"

    if [[ ${TAG:0:1} == ":" ]];
    then
        DOCKERFILE_DIR="clampax-${TAG:1}"
    else
        DOCKERFILE_DIR="clampax"
    fi

    docker build -t $REGISTRY_URL$TAG ./$DOCKERFILE_DIR

    echo "--> Pushing $REGISTRY_URL$TAG"

    docker push $REGISTRY_URL$TAG
done